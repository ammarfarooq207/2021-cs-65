﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class Student_Result : Form
    {
        public List<string> list = new List<string>();
        public Student_Result()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@StudentId,@AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(cmb_studentId.Text));
            cmd.Parameters.AddWithValue("@AssessmentComponentId", int.Parse(cmb_assessmentcmpId.Text));
            cmd.Parameters.AddWithValue("@RubricMeasurementId", int.Parse(cmb_rubricmeasurement.Text));
            cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value);

          
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Saved ..");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update StudentResult SET StudentId='" + int.Parse(cmb_studentId.Text) + "'  ,AssessmentComponentId = '" + int.Parse(cmb_assessmentcmpId.Text) + "',RubricMeasurementId = '" + int.Parse(cmb_rubricmeasurement.Text) + "',EvaluationDate = '" + dateTimePicker1.Value + "'", con);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully Updated");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from StudentResult where StudentId='" + int.Parse(cmb_studentId.Text) + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            cmb_studentId.Text = row.Cells["StudentId"].Value.ToString();
            cmb_assessmentcmpId.Text = row.Cells["AssessmentComponentId"].Value.ToString();
            cmb_rubricmeasurement.Text = row.Cells["RubricMeasurementId"].Value.ToString();
            
        }

        private void Student_Result_Load(object sender, EventArgs e)
        {
           

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                cmb_studentId.Items.Add(row["Id"].ToString());

            }

            

            var connn = Configuration.getInstance().getConnection();
            SqlCommand cmddd = new SqlCommand("Select * from RubricLevel", connn);
            SqlDataAdapter daaa = new SqlDataAdapter(cmddd);
            DataTable dttt = new DataTable();
            daaa.Fill(dttt);

            foreach (DataRow row in dttt.Rows)
            {
                cmb_rubricmeasurement.Items.Add(row["Id"].ToString());

            }

            var conec = Configuration.getInstance().getConnection();
            SqlCommand cm = new SqlCommand("Select * from Assessment", conec);
            SqlDataAdapter dat = new SqlDataAdapter(cm);
            DataTable dta = new DataTable();
            dat.Fill(dta);

            foreach (DataRow row in dta.Rows)
            {
                cmb_assessmentId.Items.Add(row["Id"].ToString());
                list.Add(row["Id"].ToString());

            }



        }

       

        private void cmb_assessmentId_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (cmb_assessmentId.Text == list[i].ToString())
                {
                    var conn = Configuration.getInstance().getConnection();
                    SqlCommand cmdd = new SqlCommand("Select * from AssessmentComponent where AssessmentId=" + cmb_assessmentId.Text + "", conn);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    cmb_assessmentcmpId.Items.Clear();
                    foreach (DataRow row in dtt.Rows)
                    {

                        cmb_assessmentcmpId.Items.Add(row["Id"].ToString());

                    }
                }
            }
        }
    }
}
