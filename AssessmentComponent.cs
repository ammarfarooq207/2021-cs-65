﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class AssessmentComponent : Form
    {
        public AssessmentComponent()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@RubricId,@AssessmentId,@Name,@TotalMarks,@DateCreated,@DateUpdated)", con);
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(comboBox2.Text));
            cmd.Parameters.AddWithValue("@Name", txt_Name.Text);
            cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(txt_TotalMarks.Text));
            cmd.Parameters.AddWithValue("@DateCreated", txt_DateCreated.Text);
            cmd.Parameters.AddWithValue("@DateUpdated", txt_DateUpdated.Text);
            
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void AssessmentComponent_Load(object sender, EventArgs e)
        {
            txt_DateCreated.Text = DateTime.Now.ToString();
            txt_DateUpdated.Text = DateTime.Now.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                comboBox1.Items.Add(row["Id"].ToString());

            }
            var conn = Configuration.getInstance().getConnection();
            SqlCommand cmdd = new SqlCommand("Select * from Assessment", conn);
            SqlDataAdapter daa = new SqlDataAdapter(cmdd);
            DataTable dtt = new DataTable();
            daa.Fill(dtt);

            foreach (DataRow row in dtt.Rows)
            {
                comboBox2.Items.Add(row["Id"].ToString());

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update AssessmentComponent SET Name='" + txt_Name.Text + "'  ,TotalMarks = '" + int.Parse(txt_TotalMarks.Text) + "',RubricId = '" + comboBox1.Text + "',AssessmentId = '" + comboBox2.Text + "',DateCreated = '" + txt_DateCreated.Text + "',DateUpdated = '" + txt_DateUpdated.Text + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Updated");
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            txt_Name.Text = row.Cells["Name"].Value.ToString();
            txt_DateCreated.Text = row.Cells["DateCreated"].Value.ToString();
            txt_DateUpdated.Text = row.Cells["DateUpdated"].Value.ToString();
            txt_TotalMarks.Text= row.Cells["TotalMarks"].Value.ToString();
            comboBox1.Text= row.Cells["RubricId"].Value.ToString();
            comboBox2.Text = row.Cells["AssessmentId"].Value.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from AssessmentComponent where AssessmentId='" + comboBox2.Text + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }
    }
}
