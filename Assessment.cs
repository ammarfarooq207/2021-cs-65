﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class Assessment : Form
    {
        public Assessment()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title,@DateCreated,@TotalMarks,@TotalWeightage)", con);
            cmd.Parameters.AddWithValue("@Title", textBox1.Text);
            cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now);
            cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(textBox2.Text));
            cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(textBox3.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Assessment SET Title='" + textBox1.Text + "'  ,TotalMarks = '" + textBox2.Text + "',TotalWeightage = '" + textBox3.Text  + "'", con);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully Updated");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Assessment where Title='" + textBox1.Text + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            textBox1.Text = row.Cells["Title"].Value.ToString();
            textBox2.Text = row.Cells["TotalMarks"].Value.ToString();
            textBox3.Text = row.Cells["TotalWeightage"].Value.ToString();
            
        }
    }
}
