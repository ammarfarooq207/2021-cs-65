﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class StudentAttendance : Form
    {
        public StudentAttendance()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Student.FirstName+' '+Student.LastName as Name, Student.RegistrationNumber ,StudentAttendance.AttendanceStatus from Student,StudentAttendance ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void ClassAttendance_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            
            foreach (DataRow row in dt.Rows)
            {
                comboBox1.Items.Add(row["Id"].ToString());
                   
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            foreach (DataGridViewRow Row in dataGridView1.Rows)
            {

                if (Row.Cells[0].Value != null)
                {
                    if ((bool)(Row.Cells[0].Value) == true)
                    {
                        bool value = Convert.ToBoolean(dataGridView1.Rows[Row.Index].Cells["Present"].Value.ToString());
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId,@StudentId,@AttendanceStatus)", con);

                        cmd.Parameters.AddWithValue("@StudentId", Row.Index + 1);

                        cmd.Parameters.AddWithValue("@AttendanceId", int.Parse(comboBox1.Text));

                        cmd.Parameters.AddWithValue("@AttendanceStatus", 1);
                        cmd.ExecuteNonQuery();

                        // Write Update Query for the ID received here
                    }
                }
                if (Row.Cells[1].Value != null)
                {
                    if ((bool)(Row.Cells[1].Value) == true)
                    {
                        bool value = Convert.ToBoolean(dataGridView1.Rows[Row.Index].Cells["Absent"].Value.ToString());
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId,@StudentId,@AttendanceStatus)", con);
                        cmd.Parameters.AddWithValue("@StudentId", Row.Index + 1);
                        cmd.Parameters.AddWithValue("@AttendanceId", int.Parse(comboBox1.Text));
                        cmd.Parameters.AddWithValue("@AttendanceStatus", 2);
                        cmd.ExecuteNonQuery();

                        // Write Update Query for the ID received here
                    }
                }
                if (Row.Cells[2].Value != null)
                {
                    if ((bool)(Row.Cells[2].Value) == true)
                    {
                        bool value = Convert.ToBoolean(dataGridView1.Rows[Row.Index].Cells["Leave"].Value.ToString());
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId,@StudentId,@AttendanceStatus)", con);
                        cmd.Parameters.AddWithValue("@StudentId", Row.Index + 1);
                        cmd.Parameters.AddWithValue("@AttendanceId", int.Parse(comboBox1.Text));
                        cmd.Parameters.AddWithValue("@AttendanceStatus", 3);
                        cmd.ExecuteNonQuery();

                        // Write Update Query for the ID received here
                    }
                }
                if (Row.Cells[3].Value != null)
                {
                    if ((bool)(Row.Cells[3].Value) == true)
                    {
                        bool value = Convert.ToBoolean(dataGridView1.Rows[Row.Index].Cells["Late"].Value.ToString());
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId,@StudentId,@AttendanceStatus)", con);
                        cmd.Parameters.AddWithValue("@StudentId", Row.Index + 1);
                        cmd.Parameters.AddWithValue("@AttendanceId", int.Parse(comboBox1.Text));
                        cmd.Parameters.AddWithValue("@AttendanceStatus", 4);
                        cmd.ExecuteNonQuery();

                        // Write Update Query for the ID received here
                    }
                }
            }
            MessageBox.Show("Attendance Marked ");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,FirstName+' '+LastName as Name , RegistrationNumber,Status from Student where Status=5", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }
    }
}
