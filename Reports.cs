﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void Reports_Load(object sender, EventArgs e)
        {

           
        }
        public void exportGridToPdf(DataGridView dgw,string filename)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

            PdfPTable pdfPTable = new PdfPTable(dgw.Columns.Count);
           
            pdfPTable.DefaultCell.Padding = 3;
            pdfPTable.WidthPercentage = 100;
            pdfPTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfPTable.DefaultCell.BorderWidth = 1;
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);

            foreach (DataGridViewColumn column in dgw.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText,font));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdfPTable.AddCell(cell);

            }

            foreach (DataGridViewRow row in dgw.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfPTable.AddCell(new Phrase(cell.Value.ToString(), font));
                }
            }
            var savefiledialog = new SaveFileDialog();
            savefiledialog.FileName = filename;
            savefiledialog.DefaultExt = ".pdf";
            if (savefiledialog.ShowDialog() == DialogResult.OK)
            {
                using(FileStream stream = new FileStream(savefiledialog.FileName, FileMode.Create))
                {
                    Document pdfdoc = new Document(PageSize.A4,10f,10f,10f,0);
                    
                    PdfWriter.GetInstance(pdfdoc, stream);
                    pdfdoc.Open();
                    
                    pdfdoc.AddTitle("REPORT");
                    pdfdoc.AddHeader("Report","Pdf Report");
                    pdfdoc.Add(pdfPTable);
                    pdfdoc.Close();
                    stream.Close();

                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            exportGridToPdf(dataGridView1, "test");
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Assessment Wise Class Result")
            {


                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Assessment.Id as Assessment, AssessmentComponent.Id as Components, AssessmentComponent.TotalMarks, StudentResult.StudentId, StudentResult.RubricMeasurementId, RubricLevel.MeasurementLevel, StudentResult.EvaluationDate,(cast(2 as decimal(10,2))/4 *AssessmentComponent.TotalMarks) as ObtainedMarks From Assessment Join AssessmentComponent on Assessment.Id = AssessmentComponent.AssessmentId  Join StudentResult on AssessmentComponent.Id = StudentResult.AssessmentComponentId Join RubricLevel on StudentResult.RubricMeasurementId = RubricLevel.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }

           else if (comboBox1.Text == "CLO Wise Class Result")
            {


                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select c.Name,r.CloID,r.ID RubricID,r.Details RubricDetails,rl.ID RubricLevelID,rl.Details RubricLevelDetails from Clo c join Rubric r on c.Id=r.CloId join RubricLevel rl on rl.RubricId=r.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }

           else if (comboBox1.Text == "Attendance Report")
            {


                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT s.FirstName, s.LastName, s.Contact, s.Email, sa.AttendanceStatus, ca.AttendanceDate FROM StudentAttendance sa JOIN ClassAttendance ca ON sa.AttendanceId = ca.Id JOIN Student s ON sa.StudentId = s.Id ORDER BY sa.AttendanceId ASC", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }

        }
    }
}
