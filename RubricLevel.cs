﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RubricLevel : Form
    {
        public RubricLevel()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@Details,@RubricId,@MeasurementLevel)", con);
            cmd.Parameters.AddWithValue("@Details", textBox2.Text);
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(comboBox1.Text));
            cmd.Parameters.AddWithValue("@MeasurementLevel", textBox5.Text);


            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update RubricLevel SET Details='" + textBox2.Text + "'  ,RubricId = '" + int.Parse(comboBox1.Text) + "',MeasurementLevel = '" + textBox5.Text + "'", con);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully Updated");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from RubricLevel where RubricId='" + comboBox1.Text + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            comboBox1.Text = row.Cells["RubricId"].Value.ToString();
            textBox2.Text = row.Cells["Details"].Value.ToString();
            textBox5.Text = row.Cells["MeasurementLevel"].Value.ToString();

        }

        private void RubricLevel_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                comboBox1.Items.Add(row["Id"].ToString());

            }
        }
    }
}
