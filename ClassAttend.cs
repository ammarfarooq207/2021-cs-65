﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class ClassAttend : Form
    {
        public ClassAttend()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var connection = Configuration.getInstance().getConnection();
            SqlCommand cmdd = new SqlCommand("Insert into ClassAttendance values (@AttendanceDate)", connection);
            cmdd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value);
            cmdd.ExecuteNonQuery();
            MessageBox.Show("Class Attendance Date Added");
        }

        private void ClassAttend_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update ClassAttendance SET AttendanceDate='" + dateTimePicker1.Value + "'", con);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully Updated");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from ClassAttendance where AttendanceDate='" + dateTimePicker1.Value + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            
            
        }
    }
}
