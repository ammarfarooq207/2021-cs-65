﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student student = new Student();
            student.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClassAttendance classAttendance = new ClassAttendance();
            classAttendance.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Assessment assessment = new Assessment();
            assessment.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CLO clo = new CLO();
            clo.Show();
        }
    }
}
