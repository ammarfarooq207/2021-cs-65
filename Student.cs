﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Student : Form
    {
        public Student()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName,@LastName,@Contact,@Email,@RegistrationNumber,@Status)", con);
            cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
            cmd.Parameters.AddWithValue("@LastName", textBox2.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox3.Text);
            cmd.Parameters.AddWithValue("@Email", textBox4.Text);

            cmd.Parameters.AddWithValue("@RegistrationNumber", textBox5.Text);
            cmd.Parameters.AddWithValue("@Status", int.Parse(textBox6.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Student SET FirstName='" + textBox1.Text + "'  ,LastName = '" + textBox2.Text + "',Contact = '" + textBox3.Text + "',Email = '" + textBox4.Text + "',RegistrationNumber = '" + textBox5.Text + "',Status = '" + textBox6.Text + "'", con);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully Updated");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Student where RegistrationNumber='" + textBox5.Text + "'", con);
            cmd.ExecuteNonQuery();
            //  ,Name = '" + txt2_txt.Text + "',Department = '" + txt_3.Text + "',Session = '" + txt_4.Text + "',CGPA = '" + txt_5.Text + "',Address = '" + txt_6.Text + "'
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.CurrentRow;
            textBox1.Text = row.Cells["FirstName"].Value.ToString();
            textBox2.Text = row.Cells["LastName"].Value.ToString();
            textBox3.Text = row.Cells["Contact"].Value.ToString();
            textBox4.Text = row.Cells["Email"].Value.ToString();

            textBox5.Text = row.Cells["RegistrationNumber"].Value.ToString();
            textBox6.Text = row.Cells["Status"].Value.ToString();
        }
    }
}
